"""
Tabulate district populations.

Hans Ekkehard Plesser, NMBU
"""

district_population = {}

with open("norway_municipalities_2017.csv", "r") as pop_file:
    pop_file.readline()  # skip header

    for line in pop_file:
        _, district, pop = line.split(',')
        if district in district_population:
            district_population[district] += int(pop)
        else:
            district_population[district] = int(pop)

# Alphabetically sorted
print(f"{'District':20}{'Population':10}")
print("-" * 30)
for district, pop in sorted(district_population.items()):
    print(f"{district:20}{pop:10d}")

# Sorted by population, brute force version with list comprehension
print()
print()
print(f"{'District':20}{'Population':10}")
print("-" * 30)
for pop, district in sorted(((p, d) for d, p in district_population.items()), reverse=True):
    print(f"{district:20}{pop:10d}")

# Sorted by population, elegant version with sort key
print()
print()
print(f"{'District':20}{'Population':10}")
print("-" * 30)
for district, pop in sorted(district_population.items(), key=lambda s: s[1], reverse=True):
    print(f"{district:20}{pop:10d}")

